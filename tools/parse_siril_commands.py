from subprocess import Popen, PIPE, STDOUT
import os, sys
import re
import pypandoc
from pathlib import Path

#########################
# get current branch name
#########################
def get_active_branch_name():
    head_dir = Path(".") / ".git" / "HEAD"
    with head_dir.open("r") as f: content = f.read().splitlines()
    for line in content:
        if line[0:4] == "ref:":
            branch = line.partition("refs/heads/")[2]
            if branch == 'main': return 'master'
            return branch

################
# get orgin repo
################
def get_repo_url():
    p = Popen('git config --get remote.origin.url', stdout=PIPE, stdin=PIPE, stderr=STDOUT, shell=True)
    log = p.communicate()[0].decode()
    baseurl = log.split('-doc.git')[0]
    if baseurl.startswith('git@gitlab.com:'):
        baseurl = baseurl.replace('git@gitlab.com:','https://gitlab.com/')
    return baseurl


#GLOBAL VARIABLES

repo_url = get_repo_url()
branchname = get_active_branch_name()
page_def = '{:s}/-/raw/{:s}/src/core/command_def.h?inline=false'.format(repo_url, branchname)
page_list = '{:s}/-/raw/{:s}/src/core/command_list.h?inline=false'.format(repo_url, branchname)

CMDFOLDER = './commands/'
loc = os.path.normpath(os.path.join(os.path.dirname(__file__),'../doc'))
os.chdir(loc)
iswin = sys.platform.startswith('win32')

#################################
# updates the command block page
# creates necessary include files
#################################
def addblock(cmdblock, cmd, usage, fmttxt, scriptable, addfile = False):
    print('Processing {:s}'.format(cmd))
    if scriptable.strip().lower() == 'true':
        script = 'scriptable'
    else:
        script = 'nonscriptable'
    # update command block
    cmdblock += ['.. index:: {:s}'.format(cmd)]
    cmdblock += ['']
    cmdblock += ['.. _{:s}:'.format(cmd)]
    cmdblock += ['']
    cmdblock += [':commandname:`{:s}`'.format(cmd)]
    cmdblock += ['|{:s}| |indexlink|'.format(script)]
    cmdblock += ['']
    if len(usage) > 0:
        cmdblock += ['']
        cmdblock += ['.. include:: {:s}{:s}_use.rst'.format(CMDFOLDER,cmd)]
        cmdblock += ['']
    if len(fmttxt) > 0:
        cmdblock += ['']
        cmdblock += ['.. include:: {:s}{:s}.rst'.format(CMDFOLDER,cmd)]
    if addfile:
        cmdblock += ['']
        cmdblock += ['.. include:: {:s}{:s}_add.rst'.format(CMDFOLDER,cmd)]
    cmdblock += ['']
    cmdblock += ['| ']
    cmdblock += ['']

    # create cmd_usage.rst
    if len(usage) > 0:
        usename = '{:s}{:s}_use.rst'.format(CMDFOLDER,cmd)
        if len(usage) == 1:
            out = '.. code-block:: text\n\n    {:s}'.format(usage[0])
        else:
            out = '.. code-block:: text\n'
            for u in usage:
                out +='\n    {:s}'.format(u)
        with open(usename,'w') as f:
            f.write(out)

    # create cmd.rst
    cmdname = '{:s}{:s}.rst'.format(CMDFOLDER,cmd)
    endline = ''
    if iswin: endline='\n'
    with open(cmdname,'w', encoding="utf8") as f:
        for l in fmttxt:
           f.write('| {:s}{:s}'.format(l,endline))

#####################
# convert html to rst
#####################
def transftxt(txt):
    out = []

    txt = txt.split('\\n')
    for t in txt:
        tmp = pypandoc.convert_text(t, 'rst', format='html', extra_args=['--wrap=none'])
        if iswin:
            tmp = tmp.replace('_\\\\"','\_"')
            tmp = tmp.replace('L*a*b\\*','L\\*a\\*b\\*')
        else:
            tmp = tmp.replace('_\\"','\_"')
            tmp = tmp.replace('L*a*b\*','L\*a\*b\*')
        tmp = tmp.replace('\\\\"','"')
        tmp = tmp.replace('\\"','"')
        tmp = tmp.replace('\r\n','')
        tmp = tmp.replace('\%22','')
        tmp = tmp.replace('See the command reference for the complete documentation on this command', '')
        out += [tmp]

    return out

################################
# create links to other commands
################################
def linkify(c):
    pattern = r'\b([A-Z]+(?:[A-Z_]+)+)\b'
    cmdname = '{:s}{:s}.rst'.format(CMDFOLDER,c)
    with open(cmdname,'r', encoding="utf8") as f:
        txt = f.readlines()
    links = []
    for t in txt:
        upperword = re.findall(pattern, t)
        if len(upperword) > 0:
            for u in upperword:
                if u.lower() in cmdlist and u.lower() != c.lower() and not u.lower() in links and not '$'+u in t:
                    links += [u.lower()]


    if len(links) > 0:
        seealso = '| \n| Links:'
        adds = [' :ref:`{:s} <{:s}>`'.format(l,l) for l in links]
        seealso += ','.join(adds)

        with open(cmdname,'a', encoding="utf8") as f:
            f.write('{:s}\n'.format(seealso))

    

######
# Main
######
if iswin:
    getcmd = "C:\msys64\msys2_shell.cmd -mingw64 -mintty -where \"{:s}\" -c \"wget -nv '{:s}' -O - | perl -0777 -pe 's/<script.*?script>//gs' | perl -pe 's/<link .*?\/>//gs' > commands_def.h\"".format(loc, page_def)
    getcmd2 = "C:\msys64\msys2_shell.cmd -mingw64 -mintty -where \"{:s}\" -c \"wget -nv '{:s}' -O - | perl -0777 -pe 's/<script.*?script>//gs' | perl -pe 's/<link .*?\/>//gs' > commands_list.h\"".format(loc, page_list)
else:
    getcmd = "wget -nv '{:s}' -O - | perl -0777 -pe 's/<script.*?script>//gs' | perl -pe 's/<link .*?\/>//gs' > {:s}/commands_def.h".format(page_def, loc)
    getcmd2 = "wget -nv '{:s}' -O - | perl -0777 -pe 's/<script.*?script>//gs' | perl -pe 's/<link .*?\/>//gs' > {:s}/commands_list.h".format(page_list, loc)


p = Popen(getcmd, stdout=PIPE, stdin=PIPE, stderr=STDOUT, shell=True)
p.communicate()
p = Popen(getcmd2, stdout=PIPE, stdin=PIPE, stderr=STDOUT, shell=True)
p.communicate()

with open(os.path.join(loc,'commands_def.h'), 'r', encoding="utf8") as fin:
    lines = fin.readlines()
with open(os.path.join(loc,'commands_list.h'), 'r', encoding="utf8") as fin:
    lines2 = fin.readlines()

cmdblock = [
        ".. role:: commandname",
        "   :class: commandname",
        ".. |indexlink| image:: ./_images/icons/index.svg",
        "               :target: genindex.html",
        "               :alt: Back to index",
        "               :width: 20px",
        ".. |scriptable| image:: ./_images/icons/scriptable.svg",
        "               :alt: Scriptable",
        ".. |nonscriptable| image:: ./_images/icons/nonscriptable.svg",
        "               :alt: Non scriptable",
        "",
        "Commands",
        "========",
        "",
        "The following page lists all the commands available in Siril |release|.",
        "",
        "You can access an index by clicking this icon |indexlink|.",
        "",
        "Commands marked with this icon |scriptable| can be used in scripts while those marked with this one |nonscriptable| cannot.",
        "",
        ".. tip::",
        "   For all the sequence commands, you can replace argument **sequencename** with a ``.`` if the sequence to be processed is already loaded."
]

flag = False
pattern = r'{(.*?)}'
pattern1 = r'\"(.*?)\", (.*?), \"(.*?)\", (.*?), (.*?), (.*?), (.* ?)'
pattern2 = r'N_\("'
cmdlist=[]
try:
    for i,l in enumerate(lines2):
        if not flag and not l.startswith('static command commands[]'):
            continue
        if not flag and l.startswith('static command commands[]'):
            flag = True
        if flag and l.startswith('\t{"') and not '{""' in l:
            flag = True
            tmp = l[2:-3]

            try:
                cmd, _, usage, _, tooltip_str, scriptable, _ = list(re.search(pattern1, tmp).groups())
            except: # multilines
                if l[-2] == '"':
                    ls = l.strip()
                    c = re.search(r'\"(.*?)\",', tmp).groups()[0]
                    r = 1
                    while c in lines2[i + r]:
                        ls += lines2[i + r].strip()
                        r +=1
                    cmd, _, usage, _, tooltip_str, scriptable, _ = list(re.search(pattern1, ls.replace('""','')).groups())

            usage = usage.replace('\\"','"')
            usage = usage.strip()
            usage = usage.replace('\\n','\n').split('\n')

            tooltip = [l for l in lines if l.startswith('#define {:s}'.format(tooltip_str))][0]
            cmdtxt = re.split(pattern2, tooltip)[1]
            cmdtxt = cmdtxt[0:-3]
            fmttxt = transftxt(cmdtxt)

            has_add_file = os.path.isfile('{:}{:s}_add.rst'.format(CMDFOLDER,cmd))

            addblock(cmdblock, cmd,usage,fmttxt,scriptable, has_add_file)
            cmdlist+= [cmd]

    # try to link the other commands by finding uppercase words
    for c in cmdlist:
        linkify(c)

except:
    print('Failed at parsing {:s}'.format(cmd))
finally:
    # create cmd.rst
    cmdname = 'Commands.rst'
    s = '\n'.join(cmdblock)
    with open(cmdname,'w') as f:
        f.write(s)
    os.remove(os.path.join(loc,'commands_def.h'))
    os.remove(os.path.join(loc,'commands_list.h'))
