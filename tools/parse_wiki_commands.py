from subprocess import Popen, PIPE, STDOUT
import os, sys
import re
import pypandoc

CMDFOLDER = './commands/'
loc = os.path.normpath(os.path.join(os.path.dirname(__file__),'../doc'))
os.chdir(loc)

#################################
# updates the command block page
# creates necessary include files
#################################
def addblock(cmdblock, cmd, usage, fmttxt):
    print('Processing {:s}'.format(cmd))
    # update command block
    cmdblock += ['.. index:: {:s}'.format(cmd)]
    cmdblock += ['']
    cmdblock += ['.. _{:s}:'.format(cmd)]
    cmdblock += ['']
    cmdblock += ['**{:s}**'.format(cmd)]
    cmdblock += ['']
    if len(usage) > 0:
        cmdblock += ['']
        cmdblock += ['.. include:: {:s}{:s}_use.rst'.format(CMDFOLDER,cmd)]
    if len(fmttxt) > 0:
        cmdblock += ['']
        cmdblock += ['.. include:: {:s}{:s}.rst'.format(CMDFOLDER,cmd)]
    cmdblock += ['']
    cmdblock += ['| ']
    cmdblock += ['']

    # create cmd_usage.rst
    if len(usage) > 0:
        usename = '{:s}{:s}_use.rst'.format(CMDFOLDER,cmd)
        out = '\n'.join(usage)
        with open(usename,'w') as f:
            f.write(out)

    # create cmd.rst
    cmdname = '{:s}{:s}.rst'.format(CMDFOLDER,cmd)
    with open(cmdname,'w', encoding="utf8") as f:
        for l in fmttxt:
           f.write('| {:s}\n'.format(l))

#####################
# convert html to rst
#####################
def transftxt(txt):
    out = []
    pattern = r'`(\S+ )<#(\S+)>`__'
    for l in txt:
        l = l.replace('<tt>','<code>')
        l = l.replace('</tt>','</code>')
        tmp = pypandoc.convert_text(l, 'rst', format='html', extra_args=['--wrap=none'])
        tmp = tmp.replace('\r','')
        tmp = tmp.replace('\n','')
        tmp = tmp.replace('_"','\_"')
        if tmp.startswith('| '):
            tmp = tmp.replace('| ','', 1)
        if tmp.startswith('::   '):
            tmp = tmp.replace('::   ',':code:`')
            tmp += "`"
        tmp = tmp.replace('</index.php?title=Siril:','<https://free-astro.org/index.php?title=Siril:')
        linkfind = re.findall(pattern, tmp)
        if len(linkfind) > 0:
            for f in linkfind:
                linkname, linkvalue = f[0], f[1]
                newlinkvalue = linkvalue.lower()
                newref = ':ref:`{:s}<{:s}>`'.format(linkname, newlinkvalue)
                oldref = '`{:s}<#{:s}>`__'.format(linkname, linkvalue)
                tmp = tmp.replace(oldref, newref)

        if len(tmp) > 0:
            out += [tmp]
    return out

######
# Main
######
if sys.platform.startswith('win32'):
    getcmd = "C:\msys64\msys2_shell.cmd -mingw64 -mintty -where \"{:s}\" -c \"wget -nv 'https://free-astro.org/index.php?title=Siril:Commands' -O - | perl -0777 -pe 's/<script.*?script>//gs' | perl -pe 's/<link .*?\/>//gs' > commands.html\"".format(loc)
else:
    getcmd = "wget -nv 'https://free-astro.org/index.php?title=Siril:Commands' -O - | perl -0777 -pe 's/<script.*?script>//gs' | perl -pe 's/<link .*?\/>//gs' > {:s}/commands.html".format(loc)

p = Popen(getcmd, stdout=PIPE, stdin=PIPE, stderr=STDOUT)
p.communicate()

with open(os.path.join(loc,'commands.html'), 'r', encoding="utf8") as fin:
    lines = fin.readlines()

cmdblock = [
        ":tocdepth: 1",
        "",
        "Commands",
        "========",
        "",
        "The following page lists all the commands available in Siril |release|.",
        "",
        "You can access an index :ref:`here <genindex>`.",
        "",
]

flag = False
pattern = r'span class="mw-headline" id="([^)]+)">'
try:
    for i,l in enumerate(lines):
        if not flag and not l.startswith('<h2>'):
            continue
        if l.startswith('<h2>'):
            flag = True
            cmd = re.search(pattern, l).groups()[0] # cmd name
            cmd = cmd.replace(',_',',')
            n = i + 1
            if lines[n].startswith('<pre>'):
                usage = ['| :code:`{:s}`'.format(lines[n].replace('<pre>','').strip())]
                while (not lines[n+1].startswith('</pre>')):
                   n+=1
                   usage += ['| :code:`{:s}`'.format(lines[n].strip())]
                n += 2
            else:
                usage =''
                n = i + 1
            cmdtxt = [lines[n].strip()]
            while (not lines[n+1].startswith('<h2>')) and (not lines[n+1].startswith('<!')):
                n+=1
                cmdtxt += [lines[n].strip()]
            fmttxt = transftxt(cmdtxt)
            addblock(cmdblock, cmd,usage,fmttxt)
except:
    print('Failed at parsing {:s}'.format(cmd))
finally:
    # create cmd.rst
    cmdname = 'Commands.rst'
    s = '\n'.join(cmdblock)
    with open(cmdname,'w') as f:
        f.write(s)
    os.remove(os.path.join(loc,'commands.html'))
