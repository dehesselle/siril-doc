Light curves
############

In astronomy, a light curve is a graph of light intensity of a celestial object
as a function of time, typically with the magnitude of light received on the y 
axis and with time on the x axis. Siril is able to generate such curves when 
analyzing stars.

There are now two ways of selecting the variable and references (also called
comparison) stars: manually, or using a list of stars obtained by the N.I.N.A.
exoplanet plugin.

Manual star selection
---------------------

Start by selecting stars and running photometry analysis on the sequence for
each, as explained :ref:`here <Photometry/quickphotometry:Quick photometry on
sequences>`.

.. figure:: ../_images/photometry/curve_stars.png
   :alt: Making a curve
   :class: with-shadow
   :width: 100%

   One star is the variable (purple with a V) and the 5 others are used as
   references.
   
.. warning::
   Make sure to not select variable stars for references. If the astrometry is
   done on your image, do not hesitate to use the :ref:`SIMBAD request <PSF_more_details>`
   to know more about the stars.
   
.. tip::
   It is preferable to choose references whose magnitude is close to that of 
   the variable.
   
Once done, Siril automatically loads the Plot tab as shown in the figure below.
This shows FWHM curves expressed as a function of frame number.

.. figure:: ../_images/photometry/curve_fwhm.png
   :alt: Making a curve
   :class: with-shadow
   :width: 100%

   The plot tab as showed right after the quick photometry on sequence.

What interests us in this part is to display the magnitude curves. Simply go to
the drop-down menu and change **FHWM** to **Magnitude**. The magnitude curves 
of each analyzed star are then displayed. This also results in the button 
:guilabel:`Light Curve` being sensitive. It is also recommended to check the
:guilabel:`Julian Date` button in order to plot magnitude as a function of a date.
   
.. figure:: ../_images/photometry/curve_mag.png
   :alt: Making a curve
   :class: with-shadow
   :width: 100%

   Switching to magnitude view make the :guilabel:`Light Curve` button sensitive.

Once the analysis is completed with a number of reference stars of at least 4 
or 5 (the higher the number, the more accurate the result), you can click on 
the :guilabel:`Light Curve` button. Siril will ask for a file name to save the data 
in ``csv`` format, then the light curve will be displayed in a new window. The 
``csv`` file can of course be used in any other software or website to reduce 
the data.

.. warning::
   As already mentionned, the software gnuplot must be installed to be able to
   see light curves.
   
.. figure:: ../_images/photometry/light_curve.svg
   :alt: Light curve
   :class: with-shadow
   :width: 100%

   Light curve of an exoplanet transit.
   
NINA exoplanet button
---------------------

In order to automate the process of transit analysis of exoplanets, lists of
reference stars, also called comparison stars, could be obtained from star
catalogues, with the appropriate criteria: similar magnitude, similar color (to
not change their relative magnitude with atmospheric extinction at different
elevations), proximity.

The capture software `N.I.N.A <https://nighttime-imaging.eu/>`_ has an
exoplanet plugin that will show such stars and allow the list to be saved in a
CSV file, such as :download:`csv file <HD_189733_b_starList.csv>`:

.. code-block:: text

   Type,Name,HFR,xPos,yPos,AvgBright,MaxBright,Background,Ra,Dec
   Target,HD 189733 b,2.6035068712769851,1992,1446,1640.3703703703704,39440,1917.0601851851852,300.18333333333328,22.709722222222222
   Var,SW Vul,2.8626145609282911,2972,276,26.14,2012,1905.445,300.02171,22.93517
   Var,DQ Vul,2.372369130017419,3006,1040,28.180555555555557,2048,1906.9027777777778,300.01254,22.78103
   Var,HQ Vul,3.8351043206620834,157,1690,49.393939393939391,2104,1905.7454545454545,300.55808,22.64067
   ...
   Comp1,ATO J300.3222+22.7056,2.4268101078425852,1367,1465,352,4496,1913.9504132231405,300.32229415181337,22.705681453738887
   Comp1,HD 189657,2.5343988482845927,2527,2808,23.814814814814813,2012,1906.5061728395062,300.08714683055996,22.4400393728
   ...
   Comp2,000-BJP-946,2.2738807043120195,1832,750,29.962962962962962,2024,1910.0648148148148,300.23741666666666,22.846999999999998
   Comp2,000-BJP-942,2.0977710589704297,2760,1572,31.083333333333332,2096,1908.6527777777778,300.025875,22.704777777777778
   ...

In the :ref:`Plot tab <Plot:Plotting Feature>`, Siril can load this file using 
the :guilabel:`NINA exoplanet` button. To use this, a few prerequisites must be
met:

* the sequence of calibrated images must be already loaded
* the reference image of the sequence must be plate solved, to make sure we 
  identify the correct stars from their equatorial J2000 coordinates
* gnuplot is installed to create or show the light curve, otherwise only the 
  data file will be created.

From there, everything is automatic, showing the light curve for the selected 
comparison stars at the end of the process.

The following video shows an automated processing of light curve with comparison star list from NINA:

.. video:: ../_static/siril_light_curve.mp4
   :alt: Cannot display video
   :width: 560

Commands and automatic operation
--------------------------------

It is also possible to automate or create the light curve remotely using the
``light_curve`` command. As blind operation needs as much automation as
possible, the configuration of the background annulus radii can be automated
with the ``-autoring`` argument: it runs a star detection in the reference
image and multiplies the mean FWHM with a configurabe factor to obtain the
inner and outer radii that should work with the sequence.

.. admonition:: Siril command line
   :class: sirilcommand
   
   .. include:: ../commands/light_curve_use.rst
   .. include:: ../commands/light_curve.rst

