| Adds the image in memory to the image **filename** given in argument.
| Result will be in 32 bits per channel if allowed in the preferences
