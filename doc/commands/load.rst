| Loads the image **filename**
| 
| It first attempts to load **filename**, then **filename**.fit, finally **filename**.fits and finally all supported formats, aborting if none of these are found.
| This scheme is applicable to every Siril command that involves reading files.
| Fits headers MIPS-HI and MIPS-LO are read and their values given to the current viewing levels.
| Writing a known extension **.ext** at the end of **filename** will load specifically the image **filename.ext**: this is used when numerous files have the same name but not the same extension
