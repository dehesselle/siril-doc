| Computes the sensor tilt as the fwhm difference between the best and worst corner truncated mean values. The **clear** option allows to clear the drawing
