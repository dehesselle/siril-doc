| Computes a linear function between a **reference** image and the image in memory.
| 
| The function is then applied to the current image to match it to the reference one. The algorithm will ignore all reference pixels whose values are outside of the [**low**, **high**] range
