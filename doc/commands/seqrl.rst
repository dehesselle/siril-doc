| The same as the **RL** command, but applies to a sequence which must be specified as the first argument
| 
| Links: :ref:`rl <rl>`
