| Stretches the image linearly to a new black point BP.
| 
| There is one mandatory argument:
| **BP** provides the new black point to stretch to.
| The argument **[channels]** may optionally be used to specify the channels to apply the stretch to: this may be R, G, B, RG, RB or GB. The default is all channels.
| Optionally the parameter **-sat** may be used to apply the linear stretch to the image saturation channel. This argument only works if all channels are selected
