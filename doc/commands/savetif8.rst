| Same command as SAVETIF but the output file is saved in 8-bit per channel: **filename**.tif. The option **-astro** allows saving in Astro-tiff format, while **-deflate** enables compression
| 
| Links: :ref:`savetif <savetif>`
