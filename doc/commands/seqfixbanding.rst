| Same command as FIXBANDING but for the sequence **sequencename**.
| 
| The output sequence name starts with the prefix "unband\_" unless otherwise specified with **-prefix=** option.
| **-vertical** option enables to perform vertical banding removal
| 
| Links: :ref:`fixbanding <fixbanding>`
