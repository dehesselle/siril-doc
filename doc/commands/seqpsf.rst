| Same command as PSF but for a sequence.
| 
| Results are displayed in the plots tab if used from the GUI, else it is printed in the console in a form that can be used to produce brightness variation curves. For headless operation, arguments are mandatory and the center of the search box in pixels can be provided with the **-at=** argument
| 
| Links: :ref:`psf <psf>`
