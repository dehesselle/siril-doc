| Extracts Ha and OIII signals from a CFA image. The output files names start with the prefix "Ha\_" and "OIII\_"
| 
| The optional argument **-resample={ha|oiii}** sets whether to upsample the Ha image or downsample the OIII image. If this argument is not provided, no resampling will be carried out and the OIII image will have twice the height and width of the Ha image
