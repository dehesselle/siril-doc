| The same as the **SB** command, but applies to a sequence which must be specified as the first argument
| 
| Links: :ref:`sb <sb>`
