| Applies a Fast Fourier Transform to the image loaded in memory. **Modulus** and **phase** given in argument are saved in FITS files
